package com.thehyve;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SimpleCompressorTest {

    @Test
    public void should_compress_valid_string() {
        String input = "0002002a0101002b";

        String output = new SimpleCompressor().compress(input);
        String expectedOutput = "022a01012b/0,2,8,";
        assertEquals(output, expectedOutput);
    }
}
