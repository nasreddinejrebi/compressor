package com.thehyve;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class CompressedDecoderTest {

    @Test
    public void should_return_valid_decoded_string() {
        String input = "00970101009803020303";

        String output = new SimpleDecoder().decode(input);
        String expectedOutput = "9797989797989797";

        assertEquals(output, expectedOutput);
    }


    @Test
    public void should_return_invalid_decoded_string_at_the_end_for_incomplete_string() {
        String input = "009701010098030203";

        String output = new SimpleDecoder().decode(input);
        String expectedOutput = "97979897973F";

        assertEquals(output, expectedOutput);
    }
}
