package com.thehyve;

public interface Compressor {
    String compress (String str);
}
