package com.thehyve;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompressedStringDecoder implements Decoder {

    private final String INVALID_DECODED_STRING = "3F";

    @Override
    public String decode(String input) {
        Compressor compressor = new SimpleCompressor();
        String[] compressedStrData = compressor.compress(input).split("/");
        assert compressedStrData.length == 2;
        String compressedStr = compressedStrData[0];
        String[] compressIndex = compressedStrData[1].split(",");


        List<String> result = new ArrayList<>();
        String hex1;
        String hex2;
        for (int i = 0; i < compressedStr.length(); ) {
            boolean isCompressedValue = Arrays.asList(compressIndex).contains(String.valueOf(i));
            int arrayMustContains = isCompressedValue ? 2 : 4;

            if (compressedStr.length() - (i + arrayMustContains) < 0) {
                result.add(INVALID_DECODED_STRING);
                return String.join("", result);
            }


            hex1 = compressedStr.substring(i, i + 2);
            if (isCompressedValue) {
                result.add(hex1);
                i += 2;
            } else {
                hex2 = compressedStr.substring(i + 2, i + 4);
                Integer b1 = Integer.valueOf(hex1, 16);
                Integer b2 = Integer.valueOf(hex2, 16);
                int returnIndex = result.size() - b1;
                if (returnIndex < 0 || returnIndex + b2 > result.size()) result.add("3F");
                else {
                    result.addAll(result.subList(returnIndex, returnIndex + b2));
                }
                i += 4;
            }
        }
        return String.join("", result);
    }
}
