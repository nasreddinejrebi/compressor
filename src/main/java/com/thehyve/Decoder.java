package com.thehyve;

public interface Decoder {

    public String decode(String input);
}
