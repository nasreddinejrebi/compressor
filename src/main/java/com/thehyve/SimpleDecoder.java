package com.thehyve;

import java.util.ArrayList;
import java.util.List;

public class SimpleDecoder implements Decoder {

    private final String INVALID_DECODED_STRING = "3F";

    public String decode(String input) {
        List<String> result = new ArrayList<>();
        String hex1;
        String hex2;
        for (int i = 0; i < input.length(); i = i + 4) {
            if (input.length() - (i + 4) < 0) {
                result.add(INVALID_DECODED_STRING);
                return String.join("", result);
            }

            hex1 = input.substring(i, i + 2);
            hex2 = input.substring(i + 2, i + 4);
            Integer b1 = Integer.valueOf(hex1, 16);
            if (b1 == 0) {
                result.add(hex2);
            } else {
                Integer b2 = Integer.valueOf(hex2, 16);
                int returnIndex = result.size() - b1;
                if (returnIndex < 0) result.add("3F");
                else {
                    if(returnIndex + b2 > result.size()) {
                        result.add("3F");
                    }else {
                        result.addAll(result.subList(returnIndex, returnIndex + b2));
                    }
                }

            }
        }

        return String.join("", result);
    }
}
