package com.thehyve;

import java.io.BufferedOutputStream;
import java.io.IOException;

public class WriteData {
    private static BufferedOutputStream out;
    private static int buffer;
    private static int n;
    private static boolean isInitialized;

    private static void initialize() {
        out = new BufferedOutputStream(System.err);
        buffer = 0;
        n = 0;
        isInitialized = true;
    }


    private static void clearBuffer() {
        if (!isInitialized) initialize();

        if (n == 0) return;
        if (n > 0) buffer <<= (8 - n);
        try {
            out.write(buffer);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        n = 0;
        buffer = 0;
    }

    private static void writeBit(boolean bit) {
        if (!isInitialized) initialize();

        // add bit to buffer
        buffer <<= 1;
        if (bit) buffer |= 1;

        // if buffer is full (8 bits), write out as a single byte
        n++;
        if (n == 8) clearBuffer();
    }


    private static void writeByte(int x) {
        if (!isInitialized) initialize();

        assert x >= 0 && x < 256;

        for (int i = 0; i < 8; i++) {
            boolean bit = ((x >>> (8 - i - 1)) & 1) == 1;
            writeBit(bit);
        }
    }


    public static void write(String str) throws IOException {
        str.chars().forEach(WriteData::writeByte);
        out.flush();
        System.out.println();
    }
}
