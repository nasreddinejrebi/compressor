package com.thehyve;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        Decoder decoder;
        // instanciate decoder based in args
        if (args.length > 0 && "1".equals(args[0])) {
            decoder = new SimpleDecoder();
        } else {
            decoder = new CompressedStringDecoder();
        }

        while (true) {
            // read From Stream
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                String input = in.readLine();
                if(input != null && !input.isEmpty()) {
                    String result = decoder.decode(input);
                    WriteData.write(result);
                }
        }
    }
}
