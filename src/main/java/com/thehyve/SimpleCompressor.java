package com.thehyve;

public class SimpleCompressor implements Compressor {

    @Override
    public String compress(String str) {
        StringBuilder compressedString = new StringBuilder();
        StringBuilder zeroIndex = new StringBuilder("/");
        int compressIndex = 0;
        for (int i = 0; i < str.length(); i = i + 4) {
            if (str.length() - (4 + i) < 0) {
                compressedString.append(str, i, str.length() - 1);
            } else {
                String subStr = str.substring(i, i + 4);
                if (subStr.startsWith("00")) {
                    compressedString.append(str, i + 2, i + 4);
                    zeroIndex.append(compressIndex).append(",");
                    compressIndex +=2;
                }else {
                    compressedString.append(str, i, i + 4);
                    compressIndex +=4;

                }
            }

        }
        compressedString.append(zeroIndex);
        return compressedString.toString();
    }
}
