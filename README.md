

### how to run the project
- first you need to build the project by running the following command: `mvn install`

- A jar will be generated and located under the directory `target` with the name `decoder.jar`

- run the program using this command `java -jar target/decoder.jar 1 or 0`:
    - 1 to run trivial
    - 0 to run with compressed data
    
 you will be able to interact with the program and perform manual tests.

- unit tests can be run with the `mvn test` command executed on the root folder of the project.